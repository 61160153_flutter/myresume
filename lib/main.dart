import 'package:flutter/material.dart';

Column _buildButtonColumn(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(icon, color: color),
      Container(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      )
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'P E R S O N A L  P R O F I L E',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                  'I am Phorntip Sudsongsan,my nickname is Por, 22 years old. Extremely motivated to constantly develop my skills and grow professionally.',
                  style: TextStyle(color: Colors.black)),
            ],
          ))
        ],
      ),
    );

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.email, 'Email'),
          _buildButtonColumn(color, Icons.facebook, 'Facebook'),
          _buildButtonColumn(color, Icons.call, 'Call'),
          _buildButtonColumn(color, Icons.map, 'Address'),
        ],
      ),
    );
    Widget textSection = Container(
      padding: EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'E D U C A T I O N',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text('Burapha University', style: TextStyle(color: Colors.black)),
              Text('Faculty of Informatic',
                  style: TextStyle(color: Colors.black)),
              Text('Computer Science', style: TextStyle(color: Colors.black)),
            ],
          ))
        ],
      ),
    );

    Widget textSection1 = Container(
      padding: EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'S K I L L S',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset(
                      'images/js.png',
                      width: 60,
                      height: 60,
                      fit: BoxFit.contain,
                    ),
                    Image.asset(
                      'images/html.png',
                      width: 60,
                      height: 60,
                      fit: BoxFit.contain,
                    ),
                    Image.asset(
                      'images/css.png',
                      width: 60,
                      height: 60,
                      fit: BoxFit.contain,
                    ),
                    Image.asset(
                      'images/mysql.png',
                      width: 60,
                      height: 60,
                      fit: BoxFit.contain,
                    ),
                    Image.asset(
                      'images/java.png',
                      width: 60,
                      height: 60,
                      fit: BoxFit.contain,
                    )
                  ],
                ),
              )
            ],
          ))
        ],
      ),
    );

    Widget textSection2 = Container(
      padding: EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'W O R K  E X P E R I E N C E',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text('DECEMBER 2020 - MARCH 2021',
                  style: TextStyle(color: Colors.black)),
              Text(
                  'My experience is learning I started studying software testing in December 2020, starting with learning documentation. Then I learned test cases and API testing.',
                  style: TextStyle(color: Colors.black)),
            ],
          ))
        ],
      ),
    );

    return MaterialApp(
      title: 'Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Resume'),
        ),
        body: ListView(children: [
          Image.asset('images/me.jpg'),
          titleSection,
          textSection,
          textSection1,
          textSection2,
          buttonSection,
        ]),
      ),
    );
  }
}
